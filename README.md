Repository for Pepper&Carrot Treasure-Hunt game translations

Pepper&Carrot treasure hunt is very simple game that can be played indoors with kids, the game consists of 16 clue cards all of them with pictures so it can be played by kids that don't know how to read.
Original from Filipe Vieira (peppercarrot/scenarios#6)

Credits:

@fsvieira creator PT and EN

@jerrywham translator FR

@cgand translator IT

@Deevad art
