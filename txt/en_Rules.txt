Add translation for Treasure Hunt rules:


Rules:

* Give one clue card to player and hide the next clues on the place indicated by the previews clue card, making a sequence of clues to the treasure card.

* Player have to find the treasure following the clues.

Note: Person/Mouse card would be someone that have information where the next clue card is, and it give it in a form  of a hint or story.
